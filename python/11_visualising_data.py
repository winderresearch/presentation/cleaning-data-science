import missingno as msno
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from pathlib import Path
import pygal
from pandas.plotting import scatter_matrix
import numpy as np
import seaborn as sns

SMALL_SIZE = 8
MEDIUM_SIZE = 12
BIGGER_SIZE = 18

matplotlib.rc('font', size=SMALL_SIZE)          # controls default text sizes
matplotlib.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
matplotlib.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y
matplotlib.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
matplotlib.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
matplotlib.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
matplotlib.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the figure title

sns.set(style="white")


def savefig(data: str, name: str):
    image_filename = filename + "_" + data + "_" + name + ".png"
    image_path = dir_path / 'images' / image_filename
    print('Saving png to {}'.format(image_path))
    plt.gcf().set_figwidth(12)
    plt.gcf().set_figheight(6)
    plt.tight_layout()
    plt.savefig(str(image_path))
    plt.close()


def savefig_titanic(name: str):
    savefig('titanic', name)


def savefig_nypd(name: str):
    savefig('nypd', name)


def savefig_hist(name: str):
    savefig('hist', name)


dir_path = Path(__file__).parent
filename = str(Path(__file__).stem)

titanic = pd.read_csv(str(dir_path / 'data' / 'original_titanic.csv'))

scatter_matrix(titanic, color='dimgray')
savefig('titanic', 'scatterplot')

# Compute the correlation matrix
corr = titanic.corr()

# Generate a mask for the upper triangle
mask = np.zeros_like(corr, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(10, 5))

# Generate a custom diverging colormap
cmap = sns.diverging_palette(220, 10, as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns.heatmap(corr, mask=mask, cmap=cmap, vmax=0.8, vmin=-0.8,
            square=True)
savefig('titanic', 'corrplot')

titanic.hist(
    color='dimgray',
    layout=(2, 4))
savefig('titanic', 'histograms')

msno.matrix(titanic.sample(500))
savefig_titanic('matrix')

msno.bar(titanic.sample(500))
savefig_titanic('bar')

msno.heatmap(titanic)
savefig_titanic('heatmap')

titanic = pd.read_csv(
    str(dir_path / 'data' / 'NYPD_Motor_Vehicle_Collisions_subset.csv'))

msno.matrix(titanic.sample(500))
savefig_nypd('matrix')

msno.bar(titanic.sample(500))
savefig_nypd('bar')

msno.heatmap(titanic)
savefig_nypd('heatmap')
