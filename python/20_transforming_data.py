from pathlib import Path

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy
import scipy.stats
import seaborn as sns
import statsmodels.api as sm
from scipy import stats
from scipy.stats import boxcox
from sklearn.svm import LinearSVC
from sklearn.metrics.pairwise import rbf_kernel
import sklearn

dir_path = Path(__file__).parent
filename = str(Path(__file__).stem)

SMALL_SIZE = 8
MEDIUM_SIZE = 12
BIGGER_SIZE = 18

matplotlib.rc('font', size=SMALL_SIZE)          # controls default text sizes
matplotlib.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
matplotlib.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y
matplotlib.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
matplotlib.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
matplotlib.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
matplotlib.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the figure title

sns.set(style="white")
sns.set_context("talk")
sns.set_palette("cubehelix")


def savefig(data: str, name: str):
    image_filename = filename + "_" + data + "_" + name + ".png"
    image_path = dir_path / 'images' / image_filename
    print('Saving png to {}'.format(image_path))
    plt.gcf().set_figwidth(12)
    plt.gcf().set_figheight(6)
    plt.tight_layout()
    plt.savefig(str(image_path))
    plt.close()


# P. J. Rousseeuw and A. M. Leroy (1987) Robust Regression and Outlier Detection. Wiley, p.27, table 3.
starsCYG = sm.datasets.get_rdataset("starsCYG", "robustbase", cache=True).data
var = 'log.Te'

ax = sns.distplot(starsCYG[var], kde=False, norm_hist=False)
ax.set(title='Star Cluster CYG OB1', xlabel='log(Temperature)', ylabel='Count')
savefig('stars', 'histogram')

stats.probplot(
    starsCYG[var], plot=sns.mpl.pyplot)
ax = plt.gca()
ax.get_lines()[0].set_markerfacecolor(sns.color_palette()[0])
ax.get_lines()[0].set_markeredgewidth(0)
ax.get_lines()[1].set_color(sns.color_palette()[1])
ax.set(title='Normal Quantile-Quantile Plot',
       xlabel='Theoretical Normal Quantiles', ylabel='Ordered Data')
savefig('stars', 'qqplot')

# Distribution fitting

y = starsCYG[var]
x_plot = scipy.linspace(min(y) - 1, max(y) + 1, 100)
y_count, x = np.histogram(y, density=True)
x = (x + np.roll(x, -1))[:-1] / 2.0
h = sns.distplot(y, kde=False, norm_hist=True)

dist_names = ['gamma', 'beta', 'rayleigh', 'norm', 'pareto', 'powernorm']

for dist_name in dist_names:
    dist = getattr(scipy.stats, dist_name)
    params = dist.fit(y)
    # Separate parts of parameters
    arg = params[:-2]
    loc = params[-2]
    scale = params[-1]
    pdf_fitted = dist.pdf(x, loc=loc, scale=scale, *arg)
    sse = np.sum(np.power(y_count - pdf_fitted, 2.0))
    pdf_plot = dist.pdf(x_plot, loc=loc, scale=scale, *arg)
    sns.lineplot(x_plot, pdf_plot, label='{} ({:.1f})'.format(dist_name, sse))
plt.legend(loc='upper right')
ax = plt.gca()
ax.set(title='Fitted Distributions (with SSE scores)',
       xlabel='log(Temperature)', ylabel='Normalised Count')
savefig('stars', 'fitting')

# Regression

sns.regplot(starsCYG['log.Te'], starsCYG['log.light'])
ax = plt.gca()
ax.set(title='Star Cluster CYG OB1 Regression',
       xlabel='log(Temperature)', ylabel='log(Light)')
savefig('stars', 'regression')

# Classification

titanic = pd.read_csv(str(dir_path / 'data' / 'original_titanic.csv'))
titanic = titanic[['survived', 'fare', 'age']]
titanic.dropna(inplace=True)
y = titanic['survived']
X = titanic[['fare', 'age']]

clf = LinearSVC(tol=1e-10, max_iter=1e5).fit(X, y)
xx, yy = np.mgrid[0:600:1, 0:100:1]
grid = np.c_[xx.ravel(), yy.ravel()]
probs = clf.predict(grid).reshape(xx.shape)
score = clf.score(X, y) * 100

sns.scatterplot(data=X, x='fare', y='age', hue=y, linewidth=0).set_title(
    "Titanic Survivors - Logistic Classification - {:.1f}%".format(score))
ax = plt.gca()
ax.contour(xx, yy, probs, cmap=plt.cm.RdYlBu)
savefig('titanic', 'classification')

# Power law data
windpower = pd.read_excel(dir_path / 'data' / "WindPowerForecastingData.xlsx")
var = 'TARGETVAR'
power = windpower[var]
power = power.dropna()
power = power.loc[power != 0]
ax = sns.distplot(power, kde=False, norm_hist=False)
ax.set(title='Wind Forecasting',
       xlabel='Normalised power output', ylabel='Count')
savefig('wind', 'histogram')

ax = sns.distplot(np.sqrt(power), kde=False, norm_hist=False)
ax.set(title='Wind Forecasting',
       xlabel='Normalised power output (sqrt)', ylabel='Count')
savefig('wind', 'histogram_sqrt')

y = np.sqrt(power)
ax = sns.distplot(y, kde=False, norm_hist=True)
ax.set(title='Wind Forecasting',
       xlabel='Normalised power output (sqrt)', ylabel='Density')
y_count, x = np.histogram(y, density=True)
x_plot = scipy.linspace(min(y), max(y), 100)
x = (x + np.roll(x, -1))[:-1] / 2.0
dist_name = 'norm'
dist = getattr(scipy.stats, dist_name)
params = dist.fit(y)
arg = params[:-2]
loc = params[-2]
scale = params[-1]
pdf_fitted = dist.pdf(x, loc=loc, scale=scale, *arg)
sse = np.sum(np.power(y_count - pdf_fitted, 2.0))
pdf_plot = dist.pdf(x_plot, loc=loc, scale=scale, *arg)
sns.lineplot(x_plot, pdf_plot, label='{} ({:.1f})'.format(dist_name, sse))
savefig('wind', 'histogram_fit')

stats.probplot(y, plot=sns.mpl.pyplot)
ax = plt.gca()
ax.get_lines()[0].set_markerfacecolor(sns.color_palette()[0])
ax.get_lines()[0].set_markeredgewidth(0)
ax.get_lines()[1].set_color(sns.color_palette()[1])
ax.set(title='Normal Quantile-Quantile Plot',
       xlabel='Theoretical Normal Quantiles', ylabel='Ordered Data')
savefig('wind', 'sqrt_qqplot')

humanpower = sm.datasets.get_rdataset("humanpower1", "DAAG", cache=True).data
var = 'wattsPerKg'
ax = sns.distplot(humanpower[var], kde=False, norm_hist=False)
ax.set(title='Oxygen uptake versus mechanical power, for humans',
       xlabel='Watts per kg', ylabel='Count')
savefig('human', 'histogram')

waves = sm.datasets.get_rdataset("waves", "HSAUR", cache=True).data
var = 'method1'
ax = sns.distplot(waves[var], kde=False, norm_hist=False)
ax.set(title='Electricity from Wave Power at Sea',
       xlabel='log(Temperature)', ylabel='Count')
savefig('waves', 'histogram')

# Fixing Classification

titanic = pd.read_csv(str(dir_path / 'data' / 'original_titanic.csv'))
titanic = titanic[['survived', 'fare', 'age']]
titanic.dropna(inplace=True)
titanic = titanic[(titanic[['fare', 'age']] != 0).all(
    axis=1)]  # Remove zero fares
y = titanic['survived']
X = titanic[['fare', 'age']]

for col in X.columns:
    ax = sns.distplot(X[col], kde=False, norm_hist=False)
    ax.set(title='Titanic Survived',
           xlabel=col, ylabel='Count')
    savefig('titanic', col)
    stats.probplot(X[col], plot=sns.mpl.pyplot)
    ax = plt.gca()
    ax.get_lines()[0].set_markerfacecolor(sns.color_palette()[0])
    ax.get_lines()[0].set_markeredgewidth(0)
    ax.get_lines()[1].set_color(sns.color_palette()[1])
    ax.set(title='Normal Quantile-Quantile Plot - {}'.format(col),
           xlabel='Theoretical Normal Quantiles', ylabel='Ordered Data')
    savefig('titanic_qqplot', col)


for func_name in ['log10', 'sqrt', 'cbrt']:
    col = 'fare'
    func = getattr(np, func_name)
    X_func = func(X[col])
    ax = sns.distplot(X_func, kde=False, norm_hist=False)
    ax.set(title='Titanic Survived',
           xlabel=col, ylabel='Count')
    savefig('titanic_{}_{}'.format(col, func_name), 'hist')
    stats.probplot(X_func, plot=sns.mpl.pyplot)
    ax = plt.gca()
    ax.get_lines()[0].set_markerfacecolor(sns.color_palette()[0])
    ax.get_lines()[0].set_markeredgewidth(0)
    ax.get_lines()[1].set_color(sns.color_palette()[1])
    ax.set(title='Normal Quantile-Quantile Plot - {} - {}'.format(col, func_name),
           xlabel='Theoretical Normal Quantiles', ylabel='Ordered Data')
    savefig('titanic_{}_{}'.format(col, func_name), 'qqplot')

# func_name = 'log10_sqrt'
# col = 'fare'
# X_func = np.sqrt(np.log10((X[col])))
# ax = sns.distplot(X_func, kde=False, norm_hist=False)
# ax.set(title='Titanic Survived',
#        xlabel=col, ylabel='Count')
# savefig('titanic_{}_{}'.format(col, func_name), 'hist')
# stats.probplot(X_func, plot=sns.mpl.pyplot)
# ax = plt.gca()
# ax.get_lines()[0].set_markerfacecolor(sns.color_palette()[0])
# ax.get_lines()[0].set_markeredgewidth(0)
# ax.get_lines()[1].set_color(sns.color_palette()[1])
# ax.set(title='Normal Quantile-Quantile Plot - {} - {}'.format(col, func_name),
#        xlabel='Theoretical Normal Quantiles', ylabel='Ordered Data')
# savefig('titanic_{}_{}'.format(col, func_name), 'qqplot')

titanic = pd.read_csv(str(dir_path / 'data' / 'original_titanic.csv'))
titanic = titanic[['survived', 'fare', 'age']]
titanic.dropna(inplace=True)
titanic = titanic[(titanic[['fare', 'age']] != 0).all(
    axis=1)]  # Remove zero fares
y = titanic['survived']
X = titanic[['fare', 'age']]

clf = LinearSVC(tol=1e-10, max_iter=1e5).fit(X, y)
xx, yy = np.mgrid[0:600:1, 0:100:1]
grid = np.c_[xx.ravel(), yy.ravel()]
probs = clf.predict(grid).reshape(xx.shape)
score = clf.score(X, y) * 100

fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=True)

sns.scatterplot(data=X, x='fare', y='age', hue=y, linewidth=0, ax=ax1).set_title(
    "Titanic Survivors - SVM Classification - {:.1f}%".format(score))
ax1.contour(xx, yy, probs, levels=[.5], cmap="Greys", vmin=0, vmax=.6)

X[col] = np.log10(X[col])
clf = LinearSVC(tol=1e-10, max_iter=1e5).fit(X, y)
xx, yy = np.mgrid[0:3:0.01, 0:100:1]
grid = np.c_[xx.ravel(), yy.ravel()]
probs = clf.predict(grid).reshape(xx.shape)
score = clf.score(X, y) * 100

sns.scatterplot(data=X, x='fare', y='age', hue=y, linewidth=0, ax=ax2).set_title(
    "Titanic Survivors - SVM Classification (log10) - {:.1f}%".format(score))
ax2.contour(xx, yy, probs, levels=[.5], cmap="Greys", vmin=0, vmax=.6)
savefig('titanic', 'classification_fixed')

# Box Cox

# Plot equation

# y = (x**lmbda - 1) / lmbda,  for lmbda > 0
#     log(x),                  for lmbda = 0


def eq_boxcox(x, lmbda):
    if lmbda == 0:
        return np.log(x)
    return (np.power(x, lmbda) - 1) / lmbda


x = scipy.linspace(0.1, 2, 100)
lambdas = scipy.arange(2, -3, -1, dtype=float)
fig = plt.figure()
df = pd.DataFrame(index=x)
for lmbda in lambdas:
    df[lmbda] = eq_boxcox(x, lmbda)
sns.lineplot(data=df)
plt.ylim(-1, 1)
plt.xlim(0, 2)
plt.title("Box-Cox Equation for values of lambda")
savefig('eq', 'boxcox')


def plot_boxcox(x):
    """Plot Box-Cox"""
    transformed, _ = boxcox(x)
    fig, axes = plt.subplots(nrows=2, ncols=2)
    sns.distplot(x, ax=axes[0, 0])
    axes[0, 0].set_title("Original")
    sns.distplot(transformed, ax=axes[0, 1])
    axes[0, 1].set_title("Transformed")
    stats.probplot(x, plot=axes[1, 0])
    axes[1, 0].get_lines()[0].set_markerfacecolor(sns.color_palette()[0])
    axes[1, 0].get_lines()[0].set_markeredgewidth(0)
    axes[1, 0].get_lines()[1].set_color(sns.color_palette()[1])
    stats.probplot(transformed, plot=axes[1, 1])
    axes[1, 1].get_lines()[0].set_markerfacecolor(sns.color_palette()[0])
    axes[1, 1].get_lines()[0].set_markeredgewidth(0)
    axes[1, 1].get_lines()[1].set_color(sns.color_palette()[1])


starsCYG = sm.datasets.get_rdataset("starsCYG", "robustbase", cache=True).data
plot_boxcox(starsCYG['log.Te'])
fig = plt.gcf()
fig.suptitle("Box-Cox - Stars - log.Te")
savefig('stars', 'boxcox')

titanic = pd.read_csv(str(dir_path / 'data' / 'original_titanic.csv'))
titanic = titanic[['survived', 'fare', 'age']]
titanic.dropna(inplace=True)
titanic = titanic[(titanic[['fare', 'age']] != 0).all(
    axis=1)]  # Remove zero fares

plot_boxcox(titanic['fare'])
fig = plt.gcf()
fig.suptitle("Box-Cox - Titanic - Fare")
savefig('titanic', 'boxcox')

# Polynomials

poly = sklearn.preprocessing.PolynomialFeatures(degree=2)

titanic = pd.read_csv(str(dir_path / 'data' / 'original_titanic.csv'))
titanic = titanic[['survived', 'fare', 'age']]
titanic.dropna(inplace=True)
titanic = titanic[(titanic[['fare', 'age']] != 0).all(
    axis=1)]  # Remove zero fares
y = titanic['survived']
X = titanic[['fare', 'age']]

clf = LinearSVC(tol=1e-10, max_iter=1e4).fit(X, y)
xx, yy = np.mgrid[0:600:1, 0:100:1]
grid = np.c_[xx.ravel(), yy.ravel()]
probs = clf.predict(grid).reshape(xx.shape)
score = clf.score(X, y) * 100

fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=True)

sns.scatterplot(data=X, x='fare', y='age', hue=y, linewidth=0, ax=ax1).set_title(
    "Titanic Survivors - SVM - {:.1f}%".format(score))
ax1.contour(xx, yy, probs, levels=[.5], cmap="Greys", vmin=0, vmax=.6)

poly_X = poly.fit_transform(X)
clf = LinearSVC(tol=1e-10, max_iter=1e5).fit(poly_X, y)
xx, yy = np.mgrid[0:600:1, 0:100:1]
grid = np.c_[xx.ravel(), yy.ravel()]
poly_grid = poly.fit_transform(grid)
probs = clf.predict(poly_grid).reshape(xx.shape)
score = clf.score(poly_X, y) * 100

sns.scatterplot(data=X, x='fare', y='age', hue=y, linewidth=0, ax=ax2).set_title(
    "Titanic Survivors - SVM (Poly Features) - {:.1f}%".format(score))
ax2.contourf(xx, yy, probs, cmap=plt.cm.BuGn, alpha=0.5)
savefig('titanic', 'classification_poly')

# Kernels

titanic = pd.read_csv(str(dir_path / 'data' / 'original_titanic.csv'))
titanic = titanic[['survived', 'fare', 'age']]
titanic.dropna(inplace=True)
titanic = titanic[(titanic[['fare', 'age']] != 0).all(
    axis=1)]  # Remove zero fares
y = titanic['survived']
X = titanic[['fare', 'age']]

clf = LinearSVC(tol=1e-10, max_iter=1e4).fit(X, y)
xx, yy = np.mgrid[0:600:1, 0:100:1]
grid = np.c_[xx.ravel(), yy.ravel()]
probs = clf.predict(grid).reshape(xx.shape)
score = clf.score(X, y) * 100

fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=True)

sns.scatterplot(data=X, x='fare', y='age', hue=y, linewidth=0, ax=ax1).set_title(
    "Titanic Survivors - SVM - {:.1f}%".format(score))
ax1.contour(xx, yy, probs, levels=[.5], cmap="Greys", vmin=0, vmax=.6)

gamma = 0.01
K = rbf_kernel(X, X, gamma=gamma)
clf = LinearSVC(tol=1e-10, max_iter=1e5).fit(K, y)
xx, yy = np.mgrid[0:600:1, 0:100:1]
grid = np.c_[xx.ravel(), yy.ravel()]
K_grid = rbf_kernel(grid, X, gamma=gamma)
probs = clf.predict(K_grid).reshape(xx.shape)
score = clf.score(K, y) * 100
print(probs)

sns.scatterplot(data=X, x='fare', y='age', hue=y, linewidth=0, ax=ax2).set_title(
    "Titanic Survivors - SVM (RBF Kernel) - {:.1f}%".format(score))
ax2.contourf(xx, yy, probs, cmap=plt.cm.BuGn, alpha=0.5)
savefig('titanic', 'classification_kernel')
